import psycopg2


def add_salaries(conn, ids, employee_ids, amount):
    cursor = conn.cursor()

    if not (type(ids) == type(employee_ids) == type(amount) == list):
        print("Error: Provide lists as parameters")
        return False

    for id in range(0, len(ids)):
        try:
            cursor.execute("INSERT INTO salaries VALUES (%s, %s, %s);", (ids[id], employee_ids[id], amount[id]))
        except Exception:
            conn.rollback()
            cursor.close()
            print("Error: incorrect id or employee_id")
            return False
    conn.commit()
    cursor.close()


connection = psycopg2.connect(
    host='localhost',
    user='postgres',
    password='postgres',
    dbname='company')

add_salaries(connection, [1006,1007], [1006,1007], [1000, 10000])